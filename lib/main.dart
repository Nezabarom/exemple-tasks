import 'package:bot_toast/bot_toast.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:exemple/ui/login_page/login_page.dart';
import 'package:exemple/utils/local_notification.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'data/repository/preferences/shared_preferences.dart';
import 'package:flutter_locales/flutter_locales.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  NotificationService().initNotification();
  await Locales.init(['en', 'uk']);
  tz.initializeTimeZones();
  SharedPreference pref = SharedPreferenceImpl();
  bool isLogin = await pref.getBoolPreferenceValue(SharedPreferenceImpl.isUserExist);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      builder: (){
        return LocaleBuilder(
          builder: (locale) {
            return MaterialApp(
              debugShowCheckedModeBanner: false,
              builder: BotToastInit(),
              navigatorObservers: [BotToastNavigatorObserver()],
              title: 'Flutter Demo',
              localizationsDelegates: Locales.delegates,
              supportedLocales: Locales.supportedLocales,
              locale: locale,
              home: const LoginPage(),
            );
          },
        );
      },
    );
  }
}