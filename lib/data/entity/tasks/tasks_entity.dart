import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/models/tasks/task_view_model.dart';
import 'package:exemple/models/tasks/tasks_response_model.dart';
import 'package:exemple/enums/priority_type_enum.dart';

abstract class TasksEntity {

  Future<TasksResponseModel?> getTasksList(int page, SortType type);

  Future<TaskViewModel?> createTask(String? title, int? dueBy, PriorityType type);

  Future<TaskViewModel?> getTaskView(int? taskID);

  Future<TaskViewModel?> editTask(int? taskID, String? title, int? dueBy, PriorityType type);

  Future<TaskViewModel?> deleteTask(int? taskID);

}