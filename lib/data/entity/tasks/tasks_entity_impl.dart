import 'package:exemple/data/entity/tasks/tasks_entity.dart';
import 'package:exemple/data/repository/network/tasks/tasks_repository.dart';
import 'package:exemple/data/repository/network/tasks/tasks_repository_impl.dart';
import 'package:exemple/data/repository/preferences/shared_preferences.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/models/auth/auth_response_model.dart';
import 'package:exemple/models/tasks/task_model.dart';
import 'package:exemple/models/tasks/task_view_model.dart';
import 'package:exemple/models/tasks/tasks_response_model.dart';
import 'package:exemple/enums/priority_type_enum.dart';

class TasksEntityImpl implements TasksEntity {

  final TasksRepository _tasksRepository = TasksRepositoryImpl();
  final SharedPreference _sharedPreference = SharedPreferenceImpl();

  @override
  Future<TasksResponseModel?> getTasksList(int page, SortType type) async {
    String token = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    return await _tasksRepository.getTasksList(page, token, type);
  }

  @override
  Future<TaskViewModel?> createTask(String? title, int? dueBy, PriorityType type) async {
    String token = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    return await _tasksRepository.createTask(token, title, dueBy, type);
  }

  @override
  Future<TaskViewModel?> getTaskView(int? taskID) async {
    String token = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    return await _tasksRepository.getTaskView(taskID, token);
  }

  @override
  Future<TaskViewModel?> editTask(int? taskID, String? title, int? dueBy, PriorityType type) async {
    String token = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    return await _tasksRepository.editTask(taskID, token, title, dueBy, type);
  }

  @override
  Future<TaskViewModel?> deleteTask(int? taskID) async {
    String token = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.token);
    return await _tasksRepository.deleteTask(taskID, token);
  }
}