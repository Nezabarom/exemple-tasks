import 'package:exemple/data/entity/auth/auth_entity.dart';
import 'package:exemple/data/repository/network/auth/auth_repository.dart';
import 'package:exemple/data/repository/network/auth/auth_repository_impl.dart';
import 'package:exemple/data/repository/preferences/shared_preferences.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:exemple/models/auth/auth_response_model.dart';


class AuthEntityImpl implements AuthEntity {

  SharedPreference sharedPreference = SharedPreferenceImpl();
  AuthRepository authRepository = AuthRepositoryImpl();

  @override
  Future<AuthResponseModel?> logIn(String email, String password) async {
    return await authRepository.logIn(email, password);
  }

  @override
  Future<AuthResponseModel?> registration(String email, String password) async {
    return await authRepository.registration(email, password);
  }

}