import 'package:exemple/models/auth/auth_response_model.dart';

abstract class AuthRepository {

  Future<AuthResponseModel?> logIn(String email, String password);
  Future<AuthResponseModel?> registration(String email, String password);

}