import 'package:exemple/data/repository/network/auth/auth_repository.dart';
import 'package:exemple/models/auth/auth_response_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class AuthRepositoryImpl implements AuthRepository {

  String baseUrl = "testapi.doitserver.in.ua";
  String baseApiUrl = "/api";

  @override
  Future<AuthResponseModel?> logIn(String email, String password) async {

    String baseApiUrl = "/api/auth";

    Map<String, String> headers = {
      "accept": "application/json",
    };

    Map<String, String> body = {
      "email": email,
      "password": password,
    };

    var uri = Uri.https( baseUrl, baseApiUrl);
    var response = await http.post(uri, headers: headers, body: body);

    print("response.statusCode login ======== ${response.statusCode}");
    print("response.body login ======== ${response.body}");

    if (response.statusCode == 200) {
      AuthResponseModel model =
      AuthResponseModel.fromJson(json.decode(response.body));
      return model;
    } else {
      return null;
    }
  }

  @override
  Future<AuthResponseModel?> registration(String email, String password) async {

    String baseApiUrl = "/api/users";

    Map<String, String> headers = {
      "accept": "application/json",
    };

    Map<String, String> body = {
      "email": email,
      "password": password,
    };

    var uri = Uri.https( baseUrl, baseApiUrl);
    var response = await http.post(uri, headers: headers, body: body);

    print("response.statusCode registration ======== ${response.statusCode}");
    print("response.body registration ======== ${response.body}");

    if (response.statusCode == 200) {
      AuthResponseModel model =
      AuthResponseModel.fromJson(json.decode(response.body));
      return model;
    } else {
      return null;
    }
  }

}