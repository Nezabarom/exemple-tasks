import 'package:exemple/data/repository/network/tasks/tasks_repository.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/models/tasks/task_view_model.dart';
import 'package:exemple/models/tasks/tasks_response_model.dart';
import 'package:exemple/enums/priority_type_enum.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class TasksRepositoryImpl implements TasksRepository {

  String baseUrl = "testapi.doitserver.in.ua";

  @override
  Future<TasksResponseModel?> getTasksList(int page, String token, SortType type) async {

    String baseApiUrl = "/api/tasks";
    late String sortType;

    switch (type){
      case SortType.titleASC:
        sortType = 'title asc';
        break;
      case SortType.titleDESC:
        sortType = 'title desc';
        break;
      case SortType.dueByASC:
        sortType = 'dueBy asc';
        break;
      case SortType.dueByDESC:
        sortType = 'dueBy desc';
        break;
      case SortType.priorityASC:
        sortType = 'priority asc';
        break;
      case SortType.priorityDESC:
        sortType = 'priority desc';
        break;
    }

    Map<String, String> queryParameters = {
      'page': '$page',
      'sort': sortType
    };

    Map<String, String> headers = {
      "accept": "application/json",
      "Authorization" : "Bearer $token",
    };

    var uri = Uri.https(baseUrl, baseApiUrl, queryParameters);
    var response = await http.get(uri, headers: headers);

    print("response.statusCode getPostsList ======== ${response.statusCode}");
    // print("response.body getPostsList ======== ${response.body}");

    if (response.statusCode == 200) {
      TasksResponseModel model =
      TasksResponseModel.fromJson(json.decode(response.body));
      return model;
    } else {
      return null;
    }
  }

  @override
  Future<TaskViewModel?> createTask(String token, String? title, int? dueBy, PriorityType type) async {

    String baseApiUrl = "/api/tasks";
    late String priority;

    if (type == PriorityType.low){
      priority = 'Low';
    }

    if (type == PriorityType.normal){
      priority = 'Normal';
    }

    if (type == PriorityType.high){
      priority = 'High';
    }

    Map<String, String> headers = {
      "accept": "application/json",
      "Authorization" : "Bearer $token",
    };

    Map<String, String> body = {
      "title" : title ?? '',
      "dueBy" : '$dueBy',
      "priority" : priority,
    };

    var uri = Uri.https( baseUrl, baseApiUrl);
    var response = await http.post(uri, headers: headers, body: body);

    print("response.statusCode createTask ======== ${response.statusCode}");
    print("response.body createTask ======== ${response.body}");

    if (response.statusCode == 200) {
      TaskViewModel? model =
      TaskViewModel?.fromJson(json.decode(response.body));
      return model;
    } else {
      return null;
    }
  }

  @override
  Future<TaskViewModel?> getTaskView(int? taskID, String token) async {

    String baseApiUrl = "/api/tasks/$taskID";

    Map<String, String> headers = {
      "accept": "application/json",
      "Authorization" : "Bearer $token",
    };

    var uri = Uri.https(baseUrl, baseApiUrl);
    var response = await http.get(uri, headers: headers);

    print("response.statusCode getTaskView ======== ${response.statusCode}");
    print("response.body getTaskView ======== ${response.body}");

    if (response.statusCode == 200) {
      TaskViewModel model =
      TaskViewModel.fromJson(json.decode(response.body));
      return model;
    } else {
      return null;
    }
  }

  @override
  Future<TaskViewModel?> editTask(int? taskID, String token, String? title, int? dueBy, PriorityType type) async {

    String baseApiUrl = "/api/tasks/$taskID";
    late String priority;
    if (type == PriorityType.low){
      priority = 'Low';
    }

    if (type == PriorityType.normal){
      priority = 'Normal';
    }

    if (type == PriorityType.high){
      priority = 'High';
    }

    Map<String, String> headers = {
      "accept": "application/json",
      "Authorization" : "Bearer $token",
    };

    Map<String, String> body = {
      "title" : title!,
      "dueBy" : '$dueBy',
      "priority" : priority,
    };

    var uri = Uri.https(baseUrl, baseApiUrl);
    var response = await http.put(uri, headers: headers, body: body);

    print("response.statusCode editTask ======== ${response.statusCode}");
    print("response.body editTask ======== ${response.body}");

    if (response.statusCode == 200) {
      TaskViewModel model =
      TaskViewModel.fromJson(json.decode(response.body));
      return model;
    } else {
      return null;
    }
  }

  @override
  Future<TaskViewModel?> deleteTask(int? taskID, String token) async {

    String baseApiUrl = "/api/tasks/$taskID";

    Map<String, String> headers = {
      "accept": "application/json",
      "Authorization" : "Bearer $token",
    };

    var uri = Uri.https(baseUrl, baseApiUrl);
    var response = await http.delete(uri, headers: headers);

    print("response.statusCode deleteTask ======== ${response.statusCode}");
    print("response.body deleteTask ======== ${response.body}");

    if (response.statusCode == 200) {
      TaskViewModel model =
      TaskViewModel.fromJson(json.decode(response.body));
      return model;
    } else {
      return null;
    }
  }

}