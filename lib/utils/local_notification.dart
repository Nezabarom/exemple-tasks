import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/timezone.dart' as tz;

class NotificationService {

  static final NotificationService _notificationService = NotificationService._interval();
  static final _notification = FlutterLocalNotificationsPlugin();

  factory NotificationService(){
    return _notificationService;
  }

  NotificationService._interval();

  Future <void> initNotification() async {
    const AndroidInitializationSettings androidInitializationSettings = AndroidInitializationSettings("@mipmap/ic_launcher");
    const IOSInitializationSettings iosInitializationSettings = IOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: false
    );

    const InitializationSettings initializationSettings = InitializationSettings(
      android: androidInitializationSettings,
      iOS: iosInitializationSettings,
    );

    await _notification.initialize(initializationSettings);
  }

  Future <void> showNotification(int taskID, String title, int duration) async {
    await _notification.zonedSchedule(
        taskID,
        "Notification",
        title,
        tz.TZDateTime.now(tz.local).add(Duration(seconds: duration)),
        const NotificationDetails(
            android: AndroidNotificationDetails("channelId", "channelName", importance: Importance.max, priority: Priority.max),
            iOS: IOSNotificationDetails(presentAlert: true, presentBadge: true, presentSound: true)
        ),
        uiLocalNotificationDateInterpretation: UILocalNotificationDateInterpretation.absoluteTime,
        androidAllowWhileIdle: true);
  }

  Future <void> removeNotification(int taskID) async {
    await _notification.cancel(taskID);
  }

  Future <void> removeAllNotification() async {
    await _notification.cancelAll();
  }
}