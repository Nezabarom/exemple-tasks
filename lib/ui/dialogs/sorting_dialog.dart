import 'package:exemple/constants/navigation_helper.dart';
import 'package:exemple/constants/ui_strings.dart';
import 'package:exemple/enums/priority_type_enum.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';

class SortingDialog {

  static void openSortingDialog(BuildContext context, SortType sortType) {
    showDialog(
      context: context,
      builder: (context) {
        return AddSortingDialog(sortType);
      },
    );
  }
}

class AddSortingDialog extends StatefulWidget {
  AddSortingDialog(this.sortType, {Key? key}) : super(key: key);

  SortType sortType;

  @override
  _AddSortingDialogState createState() => _AddSortingDialogState();
}

class _AddSortingDialogState extends State<AddSortingDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      insetPadding:
          const EdgeInsets.only(left: 235, right: 15, top: 15, bottom: 470),
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Theme.of(context).scaffoldBackgroundColor),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  LocaleText(
                    UiStrings.sortBy,
                    style: TextStyle(
                        fontSize: 17,
                        color: Colors.black.withOpacity(0.5),
                        fontWeight: FontWeight.w500),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Divider(
                    color: Theme.of(context).hintColor,
                    height: 1,
                    thickness: 1,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  _nameSortButton(),
                  const SizedBox(
                    height: 15,
                  ),
                  _prioritySortButton(),
                  const SizedBox(
                    height: 15,
                  ),
                  _dateSortButton()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _nameSortButton(){
    return InkWell(
      onTap: (){
        if(widget.sortType == SortType.titleASC){
          widget.sortType = SortType.titleDESC;
        } else {
          widget.sortType = SortType.titleASC;
        }
        NavigationHelper.openMainPage(context, widget.sortType);
      },
      child: LocaleText(
        UiStrings.nameDialog,
        style: const TextStyle(fontSize: 17, fontWeight: FontWeight.w700),
      ),
    );
  }

  Widget _prioritySortButton(){
    return InkWell(
      onTap: (){
        if(widget.sortType == SortType.priorityDESC){
          widget.sortType = SortType.priorityASC;
        } else {
          widget.sortType = SortType.priorityDESC;
        }
        NavigationHelper.openMainPage(context, widget.sortType);
      },
      child: LocaleText(
        UiStrings.priorityDialog,
        style: const TextStyle(fontSize: 17, fontWeight: FontWeight.w700),
      ),
    );
  }

  Widget _dateSortButton(){
    return InkWell(
      onTap: (){
        if(widget.sortType == SortType.dueByASC){
          widget.sortType = SortType.dueByDESC;
        } else {
          widget.sortType = SortType.dueByASC;
        }
        NavigationHelper.openMainPage(context, widget.sortType);
      },
      child: LocaleText(
        UiStrings.dateDialog,
        style: const TextStyle(fontSize: 17, fontWeight:FontWeight.w700),
      ),
    );
  }

}
