import 'package:exemple/constants/navigation_helper.dart';
import 'package:exemple/constants/ui_strings.dart';
import 'package:exemple/data/entity/tasks/tasks_entity.dart';
import 'package:exemple/data/entity/tasks/tasks_entity_impl.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/models/tasks/task_model.dart';
import 'package:exemple/ui/dialogs/sorting_dialog.dart';
import 'package:exemple/ui/widgets/loading_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';

class TasksListPage extends StatefulWidget {
  const TasksListPage(this.sortType, {Key? key}) : super(key: key);

  final SortType sortType;

  @override
  _TasksListPageState createState() => _TasksListPageState();
}

class _TasksListPageState extends State<TasksListPage> {

  final TasksEntity _tasksEntity = TasksEntityImpl();
  List<TaskModel>? taskList;
  bool isLoading = false;
  int? total;
  int? limit;
  int listPage = 1;

  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _scrollController.addListener(_scrollListener);
      _getListOfTasks();
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[300],
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: LocaleText(UiStrings.listOfTasks),
        leading: InkWell(
          onTap: (){
            NavigationHelper.openCreateTaskPage(context, widget.sortType);
          },
            child: const Icon(Icons.create_new_folder)),
        actions: [
          InkWell(
            onTap: (){
              SortingDialog.openSortingDialog(context, widget.sortType);
            },
            child: const Padding(
              padding: EdgeInsets.only(right: 15),
              child: Icon(Icons.sort),
            ),
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _getListOfTasks,
        child: (taskList != null) ? ListView.builder(
          controller: _scrollController,
          physics: const BouncingScrollPhysics(),
          itemCount: total != taskList?.length ? taskList?.length ?? 0 + 1 : taskList?.length,
            itemBuilder: (context, index){
              if (index + 1 == taskList?.length && total != taskList?.length) {
                return const Padding(
                  padding: EdgeInsets.only(bottom: 10, top: 10),
                  child: LoadingWidget(),
                );
              }
              return _taskItem(taskList![index], context);
            }) : const LoadingWidget(),
      ),
    );
  }

  Widget _taskItem (TaskModel model, BuildContext context){
    DateTime dateDueBy = DateTime(1970, 1, 1).add(Duration(seconds: model.dueBy ?? 0));
    return GestureDetector(
      onTap: (){
        int? taskID = model.id;
        NavigationHelper.openTaskDetailsPage(context, taskID, widget.sortType);
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.blueGrey
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(model.title ?? UiStrings.noData, style: const TextStyle()),
            const SizedBox(
              height: 10,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
               Row(children: [
                 LocaleText(UiStrings.dueBy),
                 const SizedBox(
                   width: 20,
                 ),
                 Text('${dateDueBy.month}/${dateDueBy.day}/${dateDueBy.year}'),
               ],),
              const SizedBox(
                width: 20,
              ),
              Text(model.priority ?? UiStrings.noData, style: const TextStyle())
            ],)
          ],
        ),
      ),
    );
  }

  Future<void> _getListOfTasks() async {
      _tasksEntity.getTasksList(1, widget.sortType).then((value) {
        taskList = value?.tasks;
        total = value?.meta.count;
        limit = value?.meta.limit;
        listPage = 1;
      setState(() {
      });
    });
  }

  Future _loadMoreList() async {
    _tasksEntity.getTasksList(listPage, widget.sortType).then((value){
      if(taskList?.length != null){
        taskList?.addAll(value?.tasks ?? []);
        setState(() {
        });
      }
    });
  }

  _scrollListener() {
    if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent){
      if(total! > taskList!.length){
        listPage ++;
        if (isLoading == false) {
          isLoading = true;
          _loadMoreList().then((value) => isLoading = false);
        }
      }
    }
  }

}
