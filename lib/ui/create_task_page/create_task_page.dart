import 'package:exemple/constants/navigation_helper.dart';
import 'package:exemple/constants/ui_strings.dart';
import 'package:exemple/constants/ui_theme.dart';
import 'package:exemple/data/entity/tasks/tasks_entity.dart';
import 'package:exemple/data/entity/tasks/tasks_entity_impl.dart';
import 'package:exemple/enums/priority_type_enum.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/utils/toasts_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';

class CreateTaskPage extends StatefulWidget {
  const CreateTaskPage(this.sortType, {Key? key}) : super(key: key);

  final SortType sortType;

  @override
  _CreateTaskPageState createState() => _CreateTaskPageState();
}

class _CreateTaskPageState extends State<CreateTaskPage> {
  final TasksEntity _tasksEntity = TasksEntityImpl();
  final TextEditingController _titleController = TextEditingController();
  TimeOfDay? _pickedTime;
  int? _taskDate;
  int? _taskTime;
  int? _timeT;
  late PriorityType _taskType;
  String? _title;
  List<bool> _priorityList = [false, false, false];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[300],
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blueGrey,
        title: LocaleText(UiStrings.createTask),
      ),
      body: Container(
        padding: const EdgeInsets.all(25),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              LocaleText(UiStrings.titleDescription),
              const SizedBox(
                height: 15,
              ),
              _titleTextField(),
              const SizedBox(
                height: 15,
              ),
              LocaleText(UiStrings.date),
              const SizedBox(
                height: 15,
              ),
              _datePicker(context),
              const SizedBox(
                height: 15,
              ),
              LocaleText(UiStrings.time),
              const SizedBox(
                height: 15,
              ),
              _timePicker(context),
              const SizedBox(
                height: 15,
              ),
              LocaleText(UiStrings.priority),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _priorityButton(0, _priorityList[0], UiStrings.low),
                  _priorityButton(1, _priorityList[1], UiStrings.normal),
                  _priorityButton(2, _priorityList[2], UiStrings.high)
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: _createButton(),
    );
  }

  Widget _titleTextField() {
    return TextField(
      controller: _titleController,
      enableInteractiveSelection: true,
      textInputAction: TextInputAction.go,
      maxLines: 3,
      autofocus: false,
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        filled: true,
        fillColor: Colors.blueGrey,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  Widget _datePicker(BuildContext context) {
    DateTime dateDueBy =
        DateTime(1970, 1, 1).add(Duration(seconds: _taskDate ?? 0));
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          child: Center(
              child: LocaleText((_taskDate != null)
                  ? '${dateDueBy.month}/${dateDueBy.day}/${dateDueBy.year}'
                  : UiStrings.notPicked)),
          height: 50,
          width: 120,
          decoration: BoxDecoration(
              color: UiTheme.firstTypeBackgroundColor,
              borderRadius: BorderRadius.circular(10)),
        ),
        InkWell(
          onTap: () {
            showDatePicker(
                    context: context,
                    initialDate: DateTime.now().add(const Duration(days: 1)),
                    firstDate: DateTime.now().add(const Duration(days: 1)),
                    lastDate: DateTime(2038, 1, 19))
                .then((data) {
              DateTime startEpochDAte = DateTime(1970, 1, 1);
              Duration? difference = data?.difference(startEpochDAte);
              setState(() {
                _taskDate = difference?.inSeconds;
              });
            });
          },
          child: Container(
            height: 40,
            width: 120,
            decoration: BoxDecoration(
                color: UiTheme.firstTypeBackgroundColor,
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: LocaleText(UiStrings.pickDate),
            ),
          ),
        )
      ],
    );
  }

  Widget _timePicker(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          child: Center(
              child: LocaleText((_pickedTime != null)
                  ? (_pickedTime!.minute > 9)
                      ? '${_pickedTime?.hour} : ${_pickedTime?.minute}'
                      : '${_pickedTime?.hour} : 0${_pickedTime?.minute}'
                  : UiStrings.notPicked)),
          height: 50,
          width: 120,
          decoration: BoxDecoration(
              color: UiTheme.firstTypeBackgroundColor,
              borderRadius: BorderRadius.circular(10)),
        ),
        InkWell(
          onTap: () {
            showTimePicker(
                    context: context,
                    initialTime: const TimeOfDay(hour: 09, minute: 00))
                .then((time) {
              Duration difference =
                  Duration(hours: time?.hour ?? 9, minutes: time?.minute ?? 0);
              setState(() {
                _taskTime = difference.inSeconds;
                _pickedTime = time;
              });
            });
          },
          child: Container(
            height: 40,
            width: 120,
            decoration: BoxDecoration(
                color: UiTheme.firstTypeBackgroundColor,
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: LocaleText(UiStrings.pickTime),
            ),
          ),
        )
      ],
    );
  }

  _selector(int index) {
    if (index == 0) {
      _priorityList = [true, false, false];
    } else if (index == 1) {
      _priorityList = [false, true, false];
    } else if (index == 2) {
      _priorityList = [false, false, true];
    }
  }

  Widget _priorityButton(int index, bool isSelected, String textButton) {
    return InkWell(
      onTap: () {
        setState(() {
          _selector(index);
        });
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(width: 3, color: Colors.blueGrey),
            color: (isSelected) ? Colors.blueGrey : Colors.transparent),
        width: 100,
        height: 25,
        child: Center(child: LocaleText(textButton)),
      ),
    );
  }

  Widget _createButton() {
    return InkWell(
      onTap: () async {
        _title = _titleController.text;
        _timeT = _taskDate! + _taskTime!;
        if (_priorityList[0] == false &&
            _priorityList[1] == false &&
            _priorityList[2] == false) {
          MessageUtils.toast(
              message: UiStrings.pickPriority, type: MessageType.warning);
        } else {
          if (_titleController.text.length >= 5 && _timeT != null) {
            if (_priorityList[0] == true) {
              _taskType = PriorityType.low;
            } else if (_priorityList[1] == true) {
              _taskType = PriorityType.normal;
            } else if (_priorityList[2] == true) {
              _taskType = PriorityType.high;
            }
            await _tasksEntity.createTask(_title, _timeT, _taskType);
            NavigationHelper.openMainPage(context, widget.sortType);
          } else {
            MessageUtils.toast(
                message: UiStrings.pickTitleDescription, type: MessageType.warning);
          }
        }
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        width: double.infinity,
        height: 50,
        decoration: BoxDecoration(
          color: Colors.blueGrey,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
            child: LocaleText(
          UiStrings.createTask,
          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
        )),
      ),
    );
  }
}
