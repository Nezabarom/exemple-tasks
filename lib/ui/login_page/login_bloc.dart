import 'dart:async';

import 'package:exemple/data/entity/auth/auth_entity.dart';
import 'package:exemple/data/entity/auth/auth_entity_impl.dart';
import 'package:exemple/data/repository/preferences/shared_preferences.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/ui/login_page/login_state.dart';

class LoginBloc {

  final _swipeButtonStreamController = StreamController<LoginPageState>.broadcast();
  Stream<LoginPageState> get swipeButtonStream => _swipeButtonStreamController.stream;

  final AuthEntity _authEntity = AuthEntityImpl();
  final SharedPreference _sharedPreference = SharedPreferenceImpl();
  bool isRegistered = true;
  SortType? sortType;

  void dispose() {
    _swipeButtonStreamController.close();
  }

 Future registration(String email, String password) async {
   await _authEntity.registration(email, password).then((value) {
     if(value != null){
       logIn(email, password);
     }
   });
 }
 
 Future <SortType?> getDefaultSorting() async {
   String? defaultSortType = await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.defaultSortType);
   if (defaultSortType == 'date') {
     sortType = SortType.dueByASC;
   }
   if (defaultSortType == 'name') {
     sortType = SortType.titleASC;
   }
   if (defaultSortType == 'priority') {
     sortType = SortType.priorityDESC;
   }
   return sortType;
 }

  Future logIn(String email, String password) async {
    await _authEntity.logIn(email, password).then((value) async {
      String? token = value?.token;
      await _sharedPreference.setStringPreferenceValue(
          SharedPreferenceImpl.token, token ?? '');
      await _sharedPreference.setBoolPreferenceValue(
          SharedPreferenceImpl.isUserExist, true);
    });
  }

  changeOnRegistrationAndBack(){
    isRegistered = !isRegistered;
    setSwipeButtonOnPage();
  }

  setSwipeButtonOnPage() {
    if (!_swipeButtonStreamController.isClosed) {
      _swipeButtonStreamController.sink.add(LoginPageState.setIsRegistered(isRegistered));
    }
  }

}