import 'package:exemple/constants/navigation_helper.dart';
import 'package:exemple/constants/ui_strings.dart';
import 'package:exemple/constants/ui_theme.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/ui/login_page/login_bloc.dart';
import 'package:exemple/ui/login_page/login_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController =
      TextEditingController(text: 'Nezabarom777@gmail.com');
  final TextEditingController _passwordController =
      TextEditingController(text: '0123456');
  late LoginBloc _bloc;

  @override
  void initState() {
    _bloc = LoginBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.secondTypeBackgroundColor,
      body: Center(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 30.w,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _emailPassTextField(_emailController, Icons.email),
                SizedBox(
                  height: 20.w,
                ),
                _emailPassTextField(_passwordController, Icons.password),
                SizedBox(
                  height: 70.w,
                ),
                _changedButtonBlockStream()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _changedButtonBlockStream() {
    return StreamBuilder(
        stream: _bloc.swipeButtonStream,
        builder: (BuildContext context, snapshot) {
          if (snapshot.data is IsRegisteredState) {
            IsRegisteredState state = snapshot.data as IsRegisteredState;
            return _changedButtonBlock(context, state.isRegistered);
          }
          return _changedButtonBlock(context, _bloc.isRegistered);
        });
  }

  Widget _changedButtonBlock(BuildContext context, bool isRegistered) {
    return Column(
      children: [
        (isRegistered)
            ? _logInOrRegistrationButton(context, isRegistered)
            : _logInOrRegistrationButton(context, isRegistered),
        SizedBox(
          height: 60.w,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            isRegistered
                ? LocaleText(
                    UiStrings.doNotHaveAccount,
                  )
                : const SizedBox(),
            SizedBox(
              width: 20.w,
            ),
            Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 10.w,
                ),
                decoration: BoxDecoration(
                  color: UiTheme.firstTypeBackgroundColor,
                  borderRadius: BorderRadius.circular(5.w),
                ),
                height: 26.w,
                child: Center(child: _switchButton(isRegistered)))
          ],
        )
      ],
    );
  }

  Widget _emailPassTextField(TextEditingController _controller, IconData icon) {
    return TextField(
      cursorColor: UiTheme.firstTypeBackgroundColor,
      controller: _controller,
      decoration: InputDecoration(
        prefixIcon: Icon(
          icon,
          color: UiTheme.firstTypeBackgroundColor,
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 20.w,
          vertical: 10.w,
        ),
        filled: true,
        fillColor: UiTheme.secondTypeBackgroundColor,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            width: 2.w,
            color: UiTheme.firstTypeBackgroundColor,
          ),
          borderRadius: BorderRadius.circular(10.w),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
            width: 2,
            color: UiTheme.firstTypeBackgroundColor,
          ),
          borderRadius: BorderRadius.circular(10.w),
        ),
      ),
    );
  }

  Widget _logInOrRegistrationButton(BuildContext context, bool isRegistered) {
    String email = _emailController.text;
    String password = _passwordController.text;
    return InkWell(
      onTap: () async {
        if (isRegistered == false) {
          _bloc.registration(email, password);
        } else {
          SortType? sortType = await _bloc.getDefaultSorting();
          _bloc.logIn(email, password).then((value) {
            NavigationHelper.openMainPage(
                context, sortType ?? SortType.dueByASC);
          });
        }
      },
      child: Container(
        width: double.infinity,
        height: 44.w,
        decoration: BoxDecoration(
          color: UiTheme.firstTypeBackgroundColor,
          borderRadius: BorderRadius.circular(10.w),
        ),
        child: Center(
            child: LocaleText(
          (isRegistered)
              ? UiStrings.loginButtonText
              : UiStrings.registrationButtonText,
          style: TextStyle(
            fontSize: 20.sp,
            fontWeight: FontWeight.w700,
          ),
        )),
      ),
    );
  }

  Widget _switchButton(bool isRegistered) {
    return InkWell(
      onTap: () {
        _bloc.changeOnRegistrationAndBack();
      },
      child: (isRegistered)
          ? LocaleText(UiStrings.toRegister)
          : LocaleText(UiStrings.toLogIn),
    );
  }
}
