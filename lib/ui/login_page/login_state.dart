class LoginPageState {
  LoginPageState();

  factory LoginPageState.setIsRegistered(bool isRegistered) = IsRegisteredState;

}

class IsRegisteredState extends LoginPageState {
  final bool isRegistered;
  IsRegisteredState(this.isRegistered);
}