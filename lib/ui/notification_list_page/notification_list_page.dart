import 'package:exemple/constants/ui_strings.dart';
import 'package:exemple/constants/ui_theme.dart';
import 'package:exemple/data/entity/tasks/tasks_entity.dart';
import 'package:exemple/data/entity/tasks/tasks_entity_impl.dart';
import 'package:exemple/data/repository/preferences/shared_preferences.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/models/tasks/task_model.dart';
import 'package:exemple/ui/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';

import '../../utils/local_notification.dart';

class NotificationListPage extends StatefulWidget {
  const NotificationListPage({Key? key}) : super(key: key);

  @override
  _NotificationListPageState createState() => _NotificationListPageState();
}

class _NotificationListPageState extends State<NotificationListPage> {
  final SharedPreference _sharedPreference = SharedPreferenceImpl();
  final TasksEntity _tasksEntity = TasksEntityImpl();
  List<String>? _notificationIdList;
  List<TaskModel>? _tasksList;
  List<TaskModel>? _trueList;
  int? _total;
  int _listPage = 1;

  @override
  void initState() {
    _getTasksListNotification();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.secondTypeBackgroundColor,
      appBar: AppBar(
        backgroundColor: UiTheme.firstTypeBackgroundColor,
        centerTitle: true,
        title: LocaleText(UiStrings.notification),
        actions: [
          InkWell(
            onTap: () {
              _removeListNotification();
            },
            child: Container(
              color: UiTheme.firstTypeBackgroundColor,
              height: 50,
              width: 100,
              child: Center(
                child: LocaleText(UiStrings.removeAll),
              ),
            ),
          )
        ],
      ),
      body: (_trueList == null)
          ? const LoadingWidget()
          : (_trueList?.isEmpty ?? true)
              ? Center(child: LocaleText(UiStrings.youDontHaveAnyNotificationsYet))
              : _notificationListBuilder(),
    );
  }

  Widget _notificationListBuilder() {
    return ListView.builder(
        itemCount: _trueList?.length,
        itemBuilder: (context, index) {
          return _notificationItem(_trueList![index], index);
        });
  }

  Widget _notificationItem(TaskModel model, int index) {
    DateTime dateDueBy = DateTime(1970, 1, 1).add(Duration(seconds: model.dueBy ?? 0));
    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.blueGrey, borderRadius: BorderRadius.circular(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded (
                  child: Column(

                    children: [
                      Text(model.title ?? ''),
                      const SizedBox(
                      height: 10,
                        ),
                      Text('${dateDueBy.month}/${dateDueBy.day}/${dateDueBy.year}')
                ],
                    crossAxisAlignment: CrossAxisAlignment.start,
              )),
              const SizedBox(
                width: 10,
              ),
              _removeButton(model.id.toString(), index)
            ],
          )
        ],
      ),
    );
  }

  Widget _removeButton(String notificationId, int index){
    return InkWell(
      onTap: (){
        _removeNotificationById(notificationId, index);
      },
      child: Container(
        height: 30,
        width: 100,
        decoration: BoxDecoration(
            color: UiTheme.darkRedColor,
            borderRadius: BorderRadius.circular(5)),
        child: Center(
          child: LocaleText(UiStrings.remove),
        ),
      ),
    );
  }

  void _removeNotificationById (String notificationId, int index) {
    if (_notificationIdList!.contains(notificationId)){
      _notificationIdList!.remove(notificationId);
      _sharedPreference.removePreferenceValue(SharedPreferenceImpl.notificationList).then((value) {
        _sharedPreference.setListStringsPreferenceValue(SharedPreferenceImpl.notificationList, _notificationIdList!);
        int notificationIdInt = int.parse(notificationId);
        NotificationService().removeNotification(notificationIdInt);
      });
      setState(() {
        _trueList!.removeAt(index);
      });
    }
  }

  void _removeListNotification() {
    _sharedPreference.removePreferenceValue(SharedPreferenceImpl.notificationList);
    NotificationService().removeAllNotification();
    setState(() {
      _trueList!.clear();
    });
  }

  _getTasksListNotification() async {
    _notificationIdList = await _sharedPreference
        .getListStringsPreferenceValue(SharedPreferenceImpl.notificationList);
    await _tasksEntity
        .getTasksList(_listPage, SortType.dueByDESC)
        .then((value) {
      _total = value?.meta.count;
      _tasksList = value?.tasks;
    });
    while (_total! > _tasksList!.length) {
      _listPage++;
      await _tasksEntity
          .getTasksList(_listPage, SortType.dueByDESC)
          .then((value) {
        _tasksList?.addAll(value?.tasks ?? []);
      });
    }
    _trueList = [];
    for (var element in _tasksList!) {
      if (_notificationIdList!.contains(element.id.toString())) {
        _trueList?.add(element);
      }
    }
    setState(() {});
  }
}
