import 'package:exemple/constants/navigation_helper.dart';
import 'package:exemple/constants/ui_strings.dart';
import 'package:exemple/constants/ui_theme.dart';
import 'package:exemple/data/entity/tasks/tasks_entity.dart';
import 'package:exemple/data/entity/tasks/tasks_entity_impl.dart';
import 'package:exemple/data/repository/preferences/shared_preferences.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/models/tasks/task_view_model.dart';
import 'package:exemple/utils/local_notification.dart';
import 'package:exemple/utils/toasts_message.dart';
import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TaskDetailsPage extends StatefulWidget {
  const TaskDetailsPage(this.taskID, this.sortType, {Key? key})
      : super(key: key);

  final int? taskID;
  final SortType sortType;

  @override
  _TaskDetailsPageState createState() => _TaskDetailsPageState();
}

class _TaskDetailsPageState extends State<TaskDetailsPage> {
  final TasksEntity _tasksEntity = TasksEntityImpl();
  final SharedPreference _sharedPreference = SharedPreferenceImpl();
  TaskViewModel? model;
  final List<int> _minuteBeforeList = [10, 20, 30, 40, 50, 60];
  int _index = 2;
  int secondInMinute = 60;

  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _getTaskView();
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DateTime dateDueBy =
        DateTime(1970, 1, 1).add(Duration(seconds: model?.task.dueBy ?? 0));
    return Scaffold(
      backgroundColor: Colors.blueGrey[300],
      appBar: AppBar(
        leading: InkWell(
            onTap: () {
              NavigationHelper.openMainPage(context, widget.sortType);
            },
            child: const Icon(Icons.arrow_back)),
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: LocaleText(
          UiStrings.taskDetails,
        ),
      ),
      body: _taskItem(context, dateDueBy),
      bottomNavigationBar: SizedBox(
        height: 120,
        child: Column(
          children: [
            _deleteOrEditTaskButton(UiStrings.editTask, true),
            _deleteOrEditTaskButton(UiStrings.deleteTask, false),
          ],
        ),
      ),
    );
  }

  Widget _taskItem(BuildContext context, DateTime dateDueBy) {
    return Container(
      margin: EdgeInsets.all(10.0.w),
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 25),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.blueGrey, borderRadius: BorderRadius.circular(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(model?.task.title ?? UiStrings.noData, style: const TextStyle()),
          const SizedBox(
            height: 10,
          ),
          const Divider(
            color: Colors.white,
            height: 0,
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            children: [
              LocaleText(UiStrings.dueBy),
              const SizedBox(
                width: 20,
              ),
              Text((model != null)
                  ? '${dateDueBy.month}/${dateDueBy.day}/${dateDueBy.year}'
                  : UiStrings.noData),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const Divider(color: Colors.white, height: 0),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              LocaleText(UiStrings.priority),
              Text(model?.task.priority ?? UiStrings.noData,
                  style: const TextStyle()),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const Divider(color: Colors.white, height: 0),
          const SizedBox(
            height: 10,
          ),
          LocaleText(UiStrings.notification),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Row(
                    children: [
                      _minuteMinusButton(),
                      Container(
                          height: 30,
                          width: 30,
                          decoration: const BoxDecoration(
                              border: Border.symmetric(
                                  horizontal: BorderSide(
                                      width: 3, color: Color(0xFF90A4AE)))),
                          child: Center(
                              child:
                                  Text(_minuteBeforeList[_index].toString()))),
                      _minutePlusButton()
                    ],
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  LocaleText(UiStrings.minuteBefore),
                ],
              ),
              _addNotificationButton(dateDueBy)
            ],
          )
        ],
      ),
    );
  }

  Widget _deleteOrEditTaskButton(String textButton, bool editTask) {
    return GestureDetector(
      onTap: () {
        if (editTask == false) {
          _deleteTask();
          NavigationHelper.openMainPage(context, widget.sortType);
        } else {
          NavigationHelper.openEditTaskPage(
              context,
              model?.task.title,
              model?.task.dueBy,
              model?.task.priority,
              widget.taskID,
              widget.sortType);
        }
      },
      child: Container(
        height: 50,
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: (editTask) ? UiTheme.firstTypeBackgroundColor : UiTheme.darkRedColor),
        child: Center(child: LocaleText(textButton)),
      ),
    );
  }

  Widget _minuteMinusButton() {
    return InkWell(
      onTap: () {
        setState(() {
          if (_index > 0) {
            _index--;
          }
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.blueGrey[300],
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(5), bottomLeft: Radius.circular(5))),
        height: 30,
        width: 30,
        child: const Center(
          child: Text('-'),
        ),
      ),
    );
  }

  Widget _minutePlusButton() {
    return InkWell(
      onTap: () async {
        setState(() {
          if (_index < 5) {
            _index++;
          }
        });
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.blueGrey[300],
            borderRadius: const BorderRadius.only(
                topRight: Radius.circular(5), bottomRight: Radius.circular(5))),
        height: 30,
        width: 30,
        child: const Center(
          child: Text('+'),
        ),
      ),
    );
  }

  Widget _addNotificationButton(DateTime dateDueBy) {
    Duration durationToReminder = dateDueBy.difference(DateTime.now());
    int durationInSeconds =
        durationToReminder.inSeconds - _minuteBeforeList[_index] * secondInMinute;
    return InkWell(
      onTap: () async {
        List<String>? notificationList =
            await _sharedPreference.getListStringsPreferenceValue(
                SharedPreferenceImpl.notificationList);
        if (!notificationList.contains(widget.taskID.toString())) {
          notificationList.add(widget.taskID.toString());
          NotificationService().showNotification(
              widget.taskID!, model!.task.title, durationInSeconds);
          _sharedPreference.setListStringsPreferenceValue(
              SharedPreferenceImpl.notificationList, notificationList);
          MessageUtils.toast(message: UiStrings.notificationAdded, type: MessageType.success);
        } else {
          MessageUtils.toast(
              message: UiStrings.notificationWasAdded, type: MessageType.info);
        }
      },
      child: Container(
        height: 30,
        width: 90,
        decoration: BoxDecoration(
            color: Colors.blueGrey[300],
            borderRadius: BorderRadius.circular(5)),
        child: Center(
          child: LocaleText(UiStrings.add),
        ),
      ),
    );
  }

  _getTaskView() {
    _tasksEntity.getTaskView(widget.taskID).then((value) {
      model = value;
      setState(() {});
    });
  }

  _deleteTask() {
    _tasksEntity.deleteTask(widget.taskID);
  }
}
