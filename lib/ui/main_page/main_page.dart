import 'package:exemple/constants/ui_theme.dart';
import 'package:exemple/data/repository/preferences/shared_preferences.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/ui/notification_list_page/notification_list_page.dart';
import 'package:exemple/ui/settings/settings_page.dart';
import 'package:exemple/ui/tasks_list_page/tasks_list_page.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  const MainPage(this.sortType,{Key? key}) : super(key: key);

  final SortType sortType;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {

  late List<Widget> _widgetOptions;
  int _selectedIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    _pageController = PageController(initialPage: _selectedIndex);
    _widgetOptions = [
      TasksListPage(widget.sortType),
      const NotificationListPage(),
      const SettingsPage()
    ];
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        children: _widgetOptions,
      ),
        bottomNavigationBar: _navBarWidget(_selectedIndex),
    );
  }

  Widget _navBarWidget(int index){
    return Theme(
      data: ThemeData(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
      ),
      child: BottomNavigationBar(
          backgroundColor: UiTheme.firstTypeBackgroundColor,
          type: BottomNavigationBarType.fixed,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.list, color: Colors.white),
              activeIcon: Icon(Icons.list, color: UiTheme.darkRedColor),
              label: 'list ot tasks',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications, color: Colors.white),
              activeIcon: Icon(Icons.notifications, color: UiTheme.darkRedColor),
              label: 'priority list',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings, color: Colors.white),
              activeIcon: Icon(Icons.settings, color: UiTheme.darkRedColor),
              label: 'settings',
            ),
          ],
      currentIndex: index,
        onTap: (int newIndex){
            _openPage(newIndex);
        },
      ),
    );
  }

  // Widget _openPageWidget(BuildContext context, int selectedIndex){
  //   return IndexedStack(
  //     children: _widgetOptions,
  //     index: _selectedIndex,
  //   );
  // }

  void _openPage(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.jumpToPage(_selectedIndex);
    });
}
}
