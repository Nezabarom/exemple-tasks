import 'package:exemple/constants/navigation_helper.dart';
import 'package:exemple/constants/ui_strings.dart';
import 'package:exemple/constants/ui_theme.dart';
import 'package:exemple/data/repository/preferences/shared_preferences.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

 @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final SharedPreference _sharedPreference = SharedPreferenceImpl();
  final List<String> _sortList = [UiStrings.nameDialog, UiStrings.dateDialog, UiStrings.priorityDialog];
  final List<String> _languageList = [UiStrings.english, UiStrings.ukrainian];
  String? _sortingDropdownValue;

  @override
  void initState() {
    getDefaultSortType();
    // WidgetsBinding.instance?.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UiTheme.secondTypeBackgroundColor,
      appBar: AppBar(
        backgroundColor: UiTheme.firstTypeBackgroundColor,
        centerTitle: true,
        title: LocaleText(UiStrings.settings),
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    flex: 5,
                    child: LocaleText(UiStrings.pickDefaultSorting)),
                Expanded(
                    flex: 3,
                    child: _defaultSortTypePicker())
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                    flex: 5,
                    child: LocaleText(UiStrings.pickLanguage)),
                Expanded(
                    flex: 3,
                    child: _languagePicker())
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: _logOutButton(),
    );
  }

  Widget _defaultSortTypePicker() {
    return DropdownButton(
      isExpanded: true,
      underline: Container(
        height: 2,
        color: UiTheme.firstTypeBackgroundColor,
      ),
      iconEnabledColor: UiTheme.firstTypeBackgroundColor,
      dropdownColor: UiTheme.firstTypeBackgroundColor,
      hint: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: LocaleText(_sortingDropdownValue ?? ''),
      ),
      items: _sortList.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: LocaleText(value),
        );
      }).toList(),
      onChanged: (String? newValue) {
        _sortingDropdownValue = newValue ?? _sortingDropdownValue;
        setNewDefaultSortType();
        setState(() {});
      },
    );
  }

  Widget _languagePicker(){
    String _languageDropdownValue = Locales.currentLocale(context)!.languageCode;
    return DropdownButton(
      isExpanded: true,
      underline: Container(
        height: 2,
        color: UiTheme.firstTypeBackgroundColor,
      ),
      iconEnabledColor: UiTheme.firstTypeBackgroundColor,
      dropdownColor: UiTheme.firstTypeBackgroundColor,
      hint: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Text(_languageDropdownValue),
      ),
      items: _languageList.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: LocaleText(value),
        );
      }).toList(),
      onChanged: (String? newValue) {
        _languageDropdownValue = newValue ?? _languageDropdownValue;
        setNewLanguage(_languageDropdownValue);
        setState(() {});
      },
    );
  }

  Widget _logOutButton() {
    return InkWell(
      onTap: () async {
        await _sharedPreference
            .removePreferenceValue(SharedPreferenceImpl.token);
        await _sharedPreference
            .removePreferenceValue(SharedPreferenceImpl.defaultSortType);
        await _sharedPreference.setBoolPreferenceValue(
            SharedPreferenceImpl.isUserExist, false);
        NavigationHelper.openLoginPage(context);
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        width: double.infinity,
        height: 50,
        decoration: BoxDecoration(
          color: UiTheme.darkRedColor,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
            child: LocaleText(
          UiStrings.logOut,
          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
        )),
      ),
    );
  }

  getDefaultSortType() async {
    _sortingDropdownValue =  await _sharedPreference.getStringPreferenceValue(SharedPreferenceImpl.defaultSortType);
    setState(() {
    });
  }

  setNewLanguage(String languageDropdownValue) {
    if(languageDropdownValue == UiStrings.english) {
      Locales.change(context, 'en');
    }
    if(languageDropdownValue == UiStrings.ukrainian) {
      Locales.change(context, 'uk');
    }
  }

  setNewDefaultSortType() async {
    _sharedPreference.removePreferenceValue(SharedPreferenceImpl.defaultSortType);
    _sharedPreference.setStringPreferenceValue(SharedPreferenceImpl.defaultSortType, _sortingDropdownValue!);
  }
}
