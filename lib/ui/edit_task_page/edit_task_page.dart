import 'package:exemple/constants/navigation_helper.dart';
import 'package:exemple/constants/ui_strings.dart';
import 'package:exemple/constants/ui_theme.dart';
import 'package:exemple/data/entity/tasks/tasks_entity.dart';
import 'package:exemple/data/entity/tasks/tasks_entity_impl.dart';
import 'package:exemple/enums/priority_type_enum.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/utils/toasts_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_locales/flutter_locales.dart';

class EditTaskPage extends StatefulWidget {
  const EditTaskPage(this.title, this.dueBy, this.priority, this.taskID, this.sortType, {Key? key}) : super(key: key);

  final String? title;
  final int? dueBy;
  final String? priority;
  final int? taskID;
  final SortType sortType;

  @override
  _EditTaskPageState createState() => _EditTaskPageState();
}

class _EditTaskPageState extends State<EditTaskPage> {

  final TasksEntity _tasksEntity = TasksEntityImpl();
  late final TextEditingController _titleController = TextEditingController(text: widget.title);
  late PriorityType _taskType;
  late String? _title = widget.title;
  late List<bool> _priorityList;
  late DateTime _taskDate = DateTime(1970, 1, 1).add(Duration(seconds: widget.dueBy ?? 0));
  late TimeOfDay _taskTime = TimeOfDay(hour: _taskDate.hour, minute: _taskDate.minute);
  late int? _timeTDate;
  late int? _timeTTime;

  @override
  void initState() {
    if (widget.priority == 'Low'){
      _priorityList = [true, false, false];
      _taskType = PriorityType.low;
    }
    else if (widget.priority == 'Normal'){
      _priorityList = [false, true, false];
      _taskType = PriorityType.normal;
    }
    else if (widget.priority == 'High'){
      _priorityList = [false, false, true];
      _taskType = PriorityType.high;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[300],
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.blueGrey,
        title: LocaleText(UiStrings.editTask),
      ),
      body: Container(
        padding: const EdgeInsets.all(25),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              LocaleText(UiStrings.titleDescription),
              const SizedBox(
                height: 15,
              ),
              _titleTextField(),
              const SizedBox(
                height: 15,
              ),
              LocaleText(UiStrings.pickDate),
              const SizedBox(
                height: 15,
              ),
              _datePicker(context),
              const SizedBox(
                height: 15,
              ),
              LocaleText(UiStrings.time),
              const SizedBox(
                height: 15,
              ),
              _timePicker(context),
              const SizedBox(
                height: 15,
              ),
              LocaleText(UiStrings.priority),
              const SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _priorityButton(0, _priorityList[0], UiStrings.low),
                  _priorityButton(1, _priorityList[1], UiStrings.normal),
                  _priorityButton(2, _priorityList[2], UiStrings.high)
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: _saveTaskButton(),
    );
  }

  Widget _titleTextField() {
    return TextField(
      controller: _titleController,
      enableInteractiveSelection: true,
      textInputAction: TextInputAction.go,
      maxLines: 3,
      autofocus: false,
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 10
        ),
        filled: true,
        fillColor: Colors.blueGrey,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(
              10),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(
              10),
        ),
      ),
    );
  }

  Widget _datePicker(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          child: Center(
              child: Text('${_taskDate.month}/${_taskDate.day}/${_taskDate.year}'
                  )),
          height: 50,
          width: 120,
          decoration: BoxDecoration(
              color: UiTheme.firstTypeBackgroundColor,
              borderRadius: BorderRadius.circular(10)),
        ),
        InkWell(
          onTap: () {
            showDatePicker(
                context: context,
                initialDate: _taskDate,
                firstDate: DateTime.now().add(const Duration(days: 1)),
                lastDate: DateTime(2038, 1, 19))
                .then((data) {
              DateTime startEpochDAte = DateTime(1970, 1, 1);
              Duration? difference = data?.difference(startEpochDAte);
              setState(() {
                _timeTDate = difference?.inSeconds;
                _taskDate = data ?? _taskDate;
              });
            });
          },
          child: Container(
            height: 40,
            width: 120,
            decoration: BoxDecoration(
                color: UiTheme.firstTypeBackgroundColor,
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: LocaleText(UiStrings.pickDate),
            ),
          ),
        )
      ],
    );
  }

  Widget _timePicker(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          child: Center(
              child: Text((_taskTime.minute > 9)
                  ? '${_taskTime.hour} : ${_taskTime.minute}'
                  : '${_taskTime.hour} : 0${_taskTime.minute}'
                  )),
          height: 50,
          width: 120,
          decoration: BoxDecoration(
              color: UiTheme.firstTypeBackgroundColor,
              borderRadius: BorderRadius.circular(10)),
        ),
        InkWell(
          onTap: () {
            showTimePicker(
                context: context,
                initialTime: const TimeOfDay(hour: 09, minute: 00))
                .then((time) {
              Duration difference =
              Duration(hours: time?.hour ?? 9, minutes: time?.minute ?? 0);
              setState(() {
                _timeTTime = difference.inSeconds;
                _taskTime = time ?? const TimeOfDay(hour: 09, minute: 00);
              });
            });
          },
          child: Container(
            height: 40,
            width: 120,
            decoration: BoxDecoration(
                color: UiTheme.firstTypeBackgroundColor,
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: LocaleText(UiStrings.pickTime),
            ),
          ),
        )
      ],
    );
  }

  _selector(int index) {
    if (index == 0) {
      _priorityList = [true, false, false];
    } else if (index == 1) {
      _priorityList = [false, true, false];
    } else if (index == 2) {
      _priorityList = [false, false, true];
    }
  }

  Widget _priorityButton(int index, bool isSelected, String textButton) {
    return InkWell(
      onTap: (){
        setState(() {
          _selector(index);
        });
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(
                width: 3,
                color: Colors.blueGrey
            ),
            color: (isSelected) ? Colors.blueGrey : Colors.transparent
        ),
        width: 100,
        height: 25,
        child: Center(child: LocaleText(textButton)),
      ),
    );
  }

  Widget _saveTaskButton() {
    return InkWell(
      onTap: () async {
        _title = _titleController.text;
        int timeT = _timeTDate! + _timeTTime!;
        if(_priorityList[0] == false && _priorityList[1] == false && _priorityList[2] == false){
          MessageUtils.toast(message: UiStrings.priority, type: MessageType.warning);
        } else {
          if(_titleController.text.length >= 5){
            if (_priorityList[0] == true){
              _taskType = PriorityType.low;
            }
            else if (_priorityList[1] == true){
              _taskType = PriorityType.normal;
            }
            else if (_priorityList[2] == true){
              _taskType = PriorityType.high;
            }
            await _tasksEntity.editTask(widget.taskID, _title, timeT, _taskType);
            NavigationHelper.openTaskDetailsPage(context, widget.taskID, widget.sortType);
          } else {
            MessageUtils.toast(message: UiStrings.pickTitleDescription, type: MessageType.warning);
          }
        }
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        width: double.infinity,
        height: 50,
        decoration: BoxDecoration(
          color: Colors.blueGrey,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
            child: LocaleText(
              UiStrings.saveTask,
              style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700
              ),
            )),
      ),
    );
  }
}
