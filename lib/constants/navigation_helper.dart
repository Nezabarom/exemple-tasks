import 'package:exemple/data/repository/preferences/shared_preferences.dart';
import 'package:exemple/data/repository/preferences/shared_preferences_impl.dart';
import 'package:exemple/enums/sort_type_enum.dart';
import 'package:exemple/ui/create_task_page/create_task_page.dart';
import 'package:exemple/ui/edit_task_page/edit_task_page.dart';
import 'package:exemple/ui/login_page/login_page.dart';
import 'package:exemple/ui/main_page/main_page.dart';
import 'package:exemple/ui/task_details_page/task_details_page.dart';
import 'package:flutter/material.dart';

class NavigationHelper {

  static void openMainPage (BuildContext context, SortType sortType) async {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => MainPage(sortType)),
          (Route<dynamic> route) => false,
    );
  }

  static void openLoginPage (BuildContext context) {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => const LoginPage()),
          (Route<dynamic> route) => false,
    );
  }

  static void openTaskDetailsPage (BuildContext context, int? taskID, SortType sortType) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => TaskDetailsPage(taskID, sortType)));
  }

  static void openCreateTaskPage (BuildContext context, SortType sortType) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => CreateTaskPage(sortType)));
  }

  static void openEditTaskPage (BuildContext context, String? title, int? dueBy, String? priority, int? taskID, SortType sortType) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => EditTaskPage(title, dueBy,priority, taskID, sortType)));
  }

}