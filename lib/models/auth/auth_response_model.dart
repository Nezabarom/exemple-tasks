class AuthResponseModel{

  AuthResponseModel({
    required this.token,
    required this.message,
  });

  String? token;
  String? message;

  factory AuthResponseModel.fromJson(Map<String, dynamic> json) => AuthResponseModel(
    token: json["token"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "token": token,
    "message": message,
  };

}