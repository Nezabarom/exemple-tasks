// To parse this JSON data, do
//
//     final tasksResponseModel = tasksResponseModelFromJson(jsonString);

import 'dart:convert';

import 'package:exemple/models/tasks/task_model.dart';

class TasksResponseModel {
  TasksResponseModel({
    required this.tasks,
    required this.meta,
  });

  List<TaskModel> tasks;
  Meta meta;

  factory TasksResponseModel.fromJson(Map<String, dynamic> json) => TasksResponseModel(
    tasks: List<TaskModel>.from(json["tasks"].map((x) => TaskModel.fromJson(x))),
    meta: Meta.fromJson(json["meta"]),
  );

  Map<String, dynamic> toJson() => {
    "tasks": List<dynamic>.from(tasks.map((x) => x.toJson())),
    "meta": meta.toJson(),
  };
}

class Meta {
  Meta({
    required this.current,
    required this.limit,
    required this.count,
  });

  int current;
  int limit;
  int count;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    current: json["current"],
    limit: json["limit"],
    count: json["count"],
  );

  Map<String, dynamic> toJson() => {
    "current": current,
    "limit": limit,
    "count": count,
  };
}