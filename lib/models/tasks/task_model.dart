class TaskModel {
  TaskModel({
    this.id,
    this.title,
    this.dueBy,
    this.priority,
  });

  int? id;
  String? title;
  int? dueBy;
  String? priority;

  factory TaskModel.fromJson(Map<String, dynamic> json) => TaskModel(
    id: json["id"],
    title: json["title"],
    dueBy: json["dueBy"],
    priority: json["priority"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "dueBy": dueBy,
    "priority": priority,
  };
}