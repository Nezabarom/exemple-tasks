import 'dart:convert';

TaskViewModel taskViewModelFromJson(String str) => TaskViewModel.fromJson(json.decode(str));

String taskViewModelToJson(TaskViewModel data) => json.encode(data.toJson());

class TaskViewModel {
  TaskViewModel({
    required this.task,
  });

  Task task;

  factory TaskViewModel.fromJson(Map<String, dynamic> json) => TaskViewModel(
    task: Task.fromJson(json["task"]),
  );

  Map<String, dynamic> toJson() => {
    "task": task.toJson(),
  };
}

class Task {
  Task({
    required this.id,
    required this.title,
    required this.dueBy,
    required this.priority,
  });

  int id;
  String title;
  int dueBy;
  String priority;

  factory Task.fromJson(Map<String, dynamic> json) => Task(
    id: json["id"],
    title: json["title"],
    dueBy: json["dueBy"],
    priority: json["priority"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "dueBy": dueBy,
    "priority": priority,
  };
}